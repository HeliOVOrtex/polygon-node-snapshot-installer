I altered the script provided by Polygon which downloads and extracts the Polygon Node Snapshot for users working with less than 6TiB SSD drives who want to download onto a larger volume, but extract into node directory on another.

All credit for the rest of the code goes to the original author.

# To make this script executable, run:
chmod +x snapshots.sh

# To execute this script with custom parameters, run:

cat snapshot.sh | bash -s -- --network mainnet --client {{bor/heimdall}} --download-dir /mnt/data0/bor/snapshots --extract-dir /var/lib/snapshots/{{bor or heimdall}} --validate-checksum false

	
